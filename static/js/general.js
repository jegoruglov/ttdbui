var jqzoom_options = {
        zoomType: 'standard',
        lens:false,
        preloadImages: false,
        alwaysOn:false,
        zoomWidth: 500,
        zoomHeight: 400,
        xOffset:50,
        yOffset:0,
        position:'left'
        //showEffect: 'fadein',
        //hideEffect: 'fadeout'
        //...MORE OPTIONS
};

DEBUG_MODE_ON = true;
if (!DEBUG_MODE_ON) {
    console = console || {};
    console.debug = function(){};
}

angular.module('ttdbui', ['ngRoute', 'ngCookies'])
  .config(['$httpProvider', '$routeProvider', function($httpProvider, $routeProvider) {
    $routeProvider.
      when('/', {controller: 'MainCtrl', templateUrl:'static/tpl/table.tpl'}).
      when('/myModal', {redirectTo: '#'}).
      otherwise({redirectTo:'/'});
  }])
  .directive('productImages', function(){
    return function(scope, element, attrs){
      var html = '';
      attrs.$observe('productImages', function(value){
        value = JSON.parse(value);
        if(attrs.imageType == 'productimage'){
          var urls = value.images ? value.images.split(',') : '';
        } else {
          var urls = value.propimages ? value.propimages.split(',') : '';
        }
        angular.forEach(urls, function(url){
          url = url.trim();
          html += '<a href="' + url + '" target="_blank" class="zoomable" title="' + value.model + '"><img src="' + url + '" class="small-image"></a>'
        });
        element.html(html);
        element.children().jqzoom(jqzoom_options);
      });
    }
  });

function MainCtrl($scope, $http, $cookies) {
  $scope.foo = {'model': undefined, 'field': undefined};
  $scope.sort_by = undefined;
  $scope.sort_by_reverse = true;
  $scope.show_archived = false;
  if($cookies.collection == undefined) {
    $scope.collection = 'pads';
    $cookies.collection = 'pads';
  } else {
    $scope.collection = $cookies.collection;
  }
  $scope.collections = undefined;
  $scope.rows = undefined;
  $scope.uploaded_rows = {};
  $scope.schema = undefined;
  $scope.uploaded_schemas = {};
  $scope.new_row = undefined;
  $scope.loadCollections = function(){
    $http.get('/query/get-collections').success(function(data){
      $scope.collections = data.data;
    });
  }
  $scope.loadCollections();
  $scope.getSchema = function(collection){
    if($scope.uploaded_schemas[collection] == undefined){
      $http.get('/query/schema/' + collection).success(function(data){
        var fields = [];
        angular.forEach(data.data.fields, function(value){
          fields.push({name: value, id: value.toLowerCase(), new_value: ''});
        });
        $scope.schema = data.data;
        $scope.schema.fields = fields;
        $scope.uploaded_schemas[collection] = $scope.schema;
      });
    } else {
      $scope.schema = $scope.uploaded_schemas[collection];
    }
  }

  $scope.getSchema($scope.collection);
  $scope.getRows = function(collection){
    if($scope.uploaded_rows[collection] == undefined){
      $http.get('/query/get/' + collection).success(function(data){
        $scope.rows = data.data;
        $scope.uploaded_rows[collection] = $scope.rows;
      });
    } else {
      $scope.rows = $scope.uploaded_rows[collection];
    }
  }
  $scope.getRows($scope.collection);

  $scope.changeRows = function(collection){
    $scope.collection = collection;
    $cookies.collection = collection;
    $scope.getSchema(collection);
    $scope.getRows(collection);
  }
  $scope.sortRows = function(field){
    $scope.sort_by = field.id;
    $scope.sort_by_reverse = !$scope.sort_by_reverse;
  }
  $scope.insertRow = function(){
    angular.element('#expandable-textarea').hide();
    new_row = {}
    angular.forEach($scope.schema.fields, function(value){
      new_row[value.id] = value.new_value;
    });
    var i = $scope.rows.push(new_row);
    i -= 1;
    $http.post('/query/insert/', data=$scope.schema).success(function(data){
      $scope.rows[i]._id = data.data._id;
      angular.forEach($scope.schema.fields, function(value){
        value.new_value = "";
      });
    });
  }
  $scope.insertRowEvent = function($event){
    if($event.keyCode == 13){
      $scope.insertRow();
    }
  }
  $scope.deleteRow = function(row){
    var index = $scope.rows.indexOf(row);
    $scope.rows.splice(index, 1);
    var data = {'collection': $scope.collection, '_id': row._id.$oid}
    $http.post('/query/delete/', body=data).success(function(data){
      //returns debug msg in data.data.msg;
    });
  }
  $scope.editRow = function(row){
    row.backup = angular.copy(row);
    row.edit = true;
  }
  $scope.updateRow = function(row){
    angular.element('#expandable-textarea').hide();
    var index = $scope.rows.indexOf(row);
    delete row.backup;
    delete row.edit;
    var new_row = {};
    angular.forEach($scope.schema.fields, function(field){
      new_row[field.id] = row[field.id];
    });
    data = {
      collection: $scope.collection,
      _id: row._id.$oid,
      data: new_row
    }
    $http.post('/query/update/', body=data).success(function(data){
      //returns debug msg in data.data.msg;
    });
  }
  $scope.updateRowEvent = function($event, row){
    if($event.keyCode == 13){
      $scope.updateRow(row);
    }
  }
  $scope.cancelEdit = function(row){
    angular.element('#expandable-textarea').hide();
    angular.forEach($scope.schema.fields, function(field){
      row[field.id] = row.backup[field.id];
    });
    delete row.backup;
    delete row.edit;
  }
  $scope.archiveRow = function(row){
    row.archived = true;
    var data = {
      _id: row._id.$oid,
      collection: $scope.collection
    };
    $http.post('/query/archive/', body=data).success(function(data){
      var a=1;
    });
  }
  $scope.dearchiveRow = function(row){
    row.archived = false;
    var data = {
      _id: row._id.$oid,
      collection: $scope.collection
    };
    $http.post('/query/dearchive/', body=data).success(function(data){
      var a=1;
    });
  }
  $scope.duplicateTextAreaShow = function(row, field){
    if(field.id == 'images' || field.id == 'description' || field.id == 'propimages'){
      console.log(field.id);
      angular.element('#expandable-textarea').show();
      var i = $scope.foo;
      if(row == undefined){
        i.model = field;
        i.field = {'id': 'new_value'};
      } else { 
        i.model = row;
        i.field = field;
      }
    }
  }
  $scope.duplicateTextAreaHide = function(field){
    console.log('Blur: ' + field.id);
    if(field.id == 'images' || field.id == 'description' || field.id == 'propimages'){
      angular.element('#expandable-textarea').hide();
    }
  }
  $scope.showTextArea = function(){
    angular.element('#expandable-textarea').show();
  }
  $scope.hideTextArea = function(){
    angular.element('#expandable-textarea').hide();
  }
  $scope.prunedString = function(s){
    if(s != undefined){
      var p = s.length > 8 ? '...' : '';
      return s.substring(8,0) + p;
    } else {
      return ''
    }
  }
}

