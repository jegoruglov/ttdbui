<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="" data-ng-click="changeRows('pads')">Donier warehouse</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="" class="dropdown-toggle" data-toggle="dropdown">Products <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li data-ng-repeat="collection in collections">
            <a role="menuitem" tabindex="-1" href="#" data-ng-click="changeRows(collection.id)">{{collection.name}}</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="#/orders">Orders</a>
      </li>
      <li>
        <a href="#/customers">Customers</a>
      </li>
      <li>
        <a href="#/accounts">Accounts</a>
      </li>
    </ul>
    <form class="navbar-form navbar-right">
      <div class="form-group" style="width: 200px;">
        <div class="input-group">
          <span class="input-group-addon"><i class="icon-search"></i></span>
          <input type="text" class="form-control input-sm" placeholder="Filter" data-ng-model="search">
        </div>
      </div>
    </form>
  </div>
</div>

<nav class="navbar navbar-default navbar-fixed-bottom navbar-inverse" style="display: none; height:100px" id="expandable-textarea">
  <div class="container">
      <div class="input-group">
        <textarea class="form-control" data-ng-focus="showTextArea()" style="height: 80px; margin-top: 10px; resize:none" data-ng-model="foo.model[foo.field.id]"></textarea>
        <span class="input-group-btn">
          <button class="btn btn-primary" type="button" data-ng-click="hideTextArea()" style="height: 80px; margin-top:10px; width:85px; font-size:20px">&raquo;</button>
        </span>
      </div>
  </div>
</nav>


<div class="container">
  <h4>{{collection.charAt(0).toUpperCase() + collection.slice(1)}}</h4>
  <table class="table table-hover table-condensed">
    <thead>
      <!-- HEADERS -->
      <tr>
        <th data-ng-repeat="field in schema.fields" class="sort-header" data-ng-click="sortRows(field)">{{field.name}}</th>
        <th style="width:100px">
          <a data-ng-href="" class="btn btn-default btn-icon" data-ng-click="show_archived = !show_archived" title="Show/hide archived articles"><i class="icon-archive"></i><i data-ng-hide="show_archived" class="icon-eye-off"></i><i data-ng-show="show_archived" class="icon-eye"></i></a>
        </th>
      </tr>
    </thead>
    <tbody>
      <!-- ROWS -->
      <tr data-ng-repeat="row in rows | filter: search | orderBy: sort_by: sort_by_reverse" data-ng-hide="row.archived && !show_archived">
        <td data-ng-repeat="field in schema.fields" data-ng-switch="row.edit">
          <div data-ng-switch-when="true">
            <div data-ng-switch="field.id">
              <input data-ng-switch-default type="text" class="form-control adjustable-input" data-ng-model="row[field.id]" data-ng-keypress="updateRowEvent($event, row)" data-ng-focus="duplicateTextAreaShow(row, field)"  data-ng-blur="duplicateTextAreaHide(field)">
            </div>
          </div>

          <div data-ng-switch-default>
            <div data-ng-switch="field.id">
              <div data-ng-switch-default>
                {{row[field.id]}}
              </div>
              <div data-ng-switch-when="description">
                {{prunedString(row[field.id])}}
              </div>
              <div data-ng-switch-when="propimages">
                <span class="product-images" product-images="{{row}}" image-type="propimage"></span>
              </div>
              <div data-ng-switch-when="images">
                <span class="product-images" product-images="{{row}}" image-type="productimage"></span>
              </div>
            </div>
          </div>
          
          
        </td>
        <td>
          <div data-ng-switch="row.edit">
            <a data-ng-href="" data-ng-switch-when="true" class="btn btn-default btn-icon" data-ng-click="updateRow(row)" title="Apply changes"><i class="icon-check"></i></a>
            <a data-ng-href="" data-ng-switch-when="true" class="btn btn-default btn-icon" data-ng-click="cancelEdit(row)" title="Cancel changes"><i class="icon-cancel"></i></a>
            <a data-ng-href="" data-ng-switch-default class="btn btn-default btn-icon" data-ng-click="editRow(row)" title="Edit article"><i class="icon-pencil"></i></a>
            <!--a data-ng-href="" data-ng-switch-default class="btn btn-default btn-icon" data-ng-click="deleteRow(row)" title="Delete article"><i class="icon-trash"></i></a-->
            <a data-ng-href="" data-ng-switch-default data-ng-show="row.archived" class="btn btn-warning btn-icon" data-ng-click="dearchiveRow(row)" title="Dearchive article"><i class="icon-archive"></i></a>
            <a data-ng-href="" data-ng-switch-default data-ng-hide="row.archived" class="btn btn-default btn-icon" data-ng-click="archiveRow(row)" title="Archive article"><i class="icon-archive"></i></a>
          </div>
        </td>
      </tr>
    </tbody>
    <tfoot>
      <!-- NEW ROW FORM -->
      <tr>
        <td data-ng-repeat="field in schema.fields">
          <input data-ng-model="field.new_value" class="form-control input-sm" data-ng-keypress="insertRowEvent($event)" data-ng-focus="duplicateTextAreaShow(undefined, field)"  data-ng-blur="duplicateTextAreaHide(field)">
        </td>
        <td><a data-ng-href="" data-ng-click="insertRow()" class="btn btn-default btn-icon" title="Add article"><i class="icon-list-add"></i></a></td>
      </tr>
    </tfoot>
  </table>

</div><!-- /.container -->