import json
from functools import partial
import pudb
import os
import urllib

import tornado.ioloop
import tornado.auth
import tornado.web
import tornado.httpserver
import tornado.httpclient
import tornado.autoreload
import tornado.options

import conf

settings = {
    'debug': True,
    'secure_cookie_expire_days': None,#1.0/60/60/24 * 5,
}

class BaseHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.cli = tornado.httpclient.AsyncHTTPClient()
        self.mongo_api_key = conf.mongo_api_key

class MainHandler(BaseHandler):
    def get(self):
        with open('index.html') as f:
            html = f.read()
        self.write(html)

class QueryHandler(BaseHandler):
    @tornado.web.asynchronous
    def get(self, query, body=None):
        if query:
            if self.request.host in conf.mogno_queries_authorized_hosts:
                url = '{host}/{path}?key={key}'.format(
                    host=conf.mongo_api_host.strip('/'),
                    path=urllib.quote(query.encode('utf8')),
                    key=self.mongo_api_key
                )
                cb = partial(self._on_response, query=query)
                if body:
                    self.cli.fetch(url, cb, method='POST', body=body)
                else:
                    self.cli.fetch(url, cb)
            else:
                self.finish({
                    'status': -1,
                    'data': '',
                    'msg': 'Unauthorized host'
                })

    def _on_response(self, response, query=None):
        response = not isinstance(response.body, basestring) and response.body or json.loads(response.body)
        if isinstance(response, dict) and 'status' in response:
            if response['status'] == 0:
                self.finish({
                    'status': 0,
                    'data': response['data'],
                    'msg': u'Query successfully executed: "{}". Response: {}'.format(query, response['data'])
                })
            else:
                self.finish({
                    'status': -1,
                    'data': None,
                    'msg': response['msg']
                })
        else:
            self.finish({
                'status': 0,
                'data': response,
                'msg': u'Query successfully executed: "{}". Response: {}'.format(query, response)
            })

    @tornado.web.asynchronous
    def post(self, query):
        self.get(query, self.request.body)

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/static/(.*)", tornado.web.StaticFileHandler, {
        "path": os.path.join(os.path.dirname(__file__), "static"),
    }),
    (r"/query/(.*)", QueryHandler)
], **settings)
tornado.options.define("port", default=9919, help="run on the given port", type=int)
tornado.options.parse_command_line()
http_server = tornado.httpserver.HTTPServer(application)
http_server.listen(tornado.options.options.port)
tornado.autoreload.start()
tornado.ioloop.IOLoop.instance().start()
